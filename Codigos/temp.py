# Includes
from skimage.transform import radon, iradon
import numpy as np
import matplotlib.pyplot as plt
import pydicom
import pylibjpeg
from pydicom.data import get_testdata_file
import os
import pandas as pd
from skimage.util import random_noise

def read_dcm(img_name):
    os.chdir('images_dcm/')
    filename = get_testdata_file(img_name)
    dataset = pydicom.dcmread(img_name)
    os.chdir('../')
    plt.imshow(dataset.pixel_array, cmap="gray")
    plt.show()
    return dataset.pixel_array

def Radon(image, number_angles = 500):
    angles = np.linspace(0, 180, number_angles)
    sinogram = radon(image, theta = angles, circle = False)#, preserve_range=True)
    return sinogram

def Iradon(sinogram, number_angles = 500):
    angles = np.linspace(0, 180, number_angles)
    fbp = iradon(sinogram, theta = angles, circle = False)
    return fbp

def hu2mu(img, mu_water, mu_air):
    img = img/1000.0*(mu_water-mu_air) + mu_water
    return img

def mu2hu(img, mu_water, mu_air):
    img = 1000*(img-mu_water)/(mu_water-mu_air)
    return img

def threshold_based_weighting(img, T1, T2):
    w_bone = (img - T1) / (T2 - T1)
    w_bone = np.clip(w_bone, 0 , 1)
    bone = w_bone * img

    w_water = (T2 - img) / (T2 - T1)
    w_water = np.clip(w_water, 0 , 1)
    water = w_water * img
    
    return [water, bone]

dataset = pydicom.dcmread('images_dcm/CT000209.dcm') #adquirindo dados do dicom
# 1º passo - iniciar
img = read_dcm('CT000209.dcm') #lendo imagem real
img_metal = np.zeros(img.shape) #criando espaço para o metal
img_metal[300:320, 300:320] = 1
pixel_size = float(dataset['PixelSpacing'][0]) #the real size of each pixel [cm]

data = pd.read_csv('xray_characteristic_data.csv')
E0 = 40-1
mu_air = 0
mu_water = data.iloc[E0]['Water']

# 3º Passo - pré-processamento
img[img <= -2000] = 0
img = img - 1000
img = hu2mu(img, mu_water, mu_air)

# 5º passo
data = pd.read_csv('xray_characteristic_data.csv')
energy_composition = np.linspace(0,119, 120).astype(int)
E0 = 40-1
mu_air = 0
metal_name = 'Titanium'
metal_density = 6
T1 = 100
T2 = 1500
noise_scale = 12
filter_name = 'Hann'
freqscale = 1

SOD = 50/pixel_size
angle_size = 0.1
angle_num = 1000
pixel_size = float(dataset['PixelSpacing'][0])
output_size = img.shape[0]

m0_water = data.iloc[E0]['Water']
m0_bone = data.iloc[E0]['Bone']
m0_metal = data.iloc[E0][metal_name]
mu_water0 = m0_water * 1.0
mu_metal0 = m0_metal * metal_density

T1 = hu2mu(T1, mu_water0, mu_air)
T2 = hu2mu(T2, mu_water0, mu_air)

[x_water, x_bone] = threshold_based_weighting(img, T1, T2)
x_bone[img_metal>0] = 0
x_water[img_metal>0] = 0
img_metal = img_metal * mu_metal0

d_water = Radon(x_water, angle_num)
d_bone = Radon(x_bone, angle_num)
d_metal = Radon(img_metal, angle_num)

d_metal = d_metal * pixel_size
d_bone = d_bone * pixel_size
d_water = d_water * pixel_size

total_intensity = 0
v = np.zeros((len(energy_composition), d_water.shape[0], d_water.shape[1]))

for i in energy_composition:
    m_water = data.iloc[i]['Water']
    m_bone = data.iloc[i]['Bone']
    m_metal = data.iloc[i][metal_name]
    intensity = data.iloc[i]['Intensity']
    d_water_tmp = d_water*(m_water/m0_water)
    d_bone_tmp = d_bone*(m_bone/m0_bone)
    d_metal_tmp = d_metal*(m_metal/m0_metal)
    DRR = d_water_tmp + d_bone_tmp + d_metal_tmp
    
    y = intensity * (np.exp(-DRR))
    v[int(i), :, :] = y
    total_intensity = total_intensity + intensity
    
poly_y = np.sum(v,0)
noisy_y = (10**noise_scale) * random_noise(poly_y/(10**noise_scale), mode='poisson')
p_final = -np.log(noisy_y/total_intensity)

sim = Iradon(p_final, angle_num)
sim[sim < 0] = 0
sim = sim / pixel_size

mu_air = 0
mu_water = data.iloc[E0]['Water']

output = mu2hu(sim, mu_water, mu_air)
plt.imshow(output, cmap='gray')