import numpy as np
import pandas as pd
from scipy.special import expit
from skimage.transform import radon, iradon


def Radon(image, number_angles = 500):
    angles = np.linspace(0, 180, number_angles)
    sinogram = radon(image, theta = angles, circle = False, preserve_range=True)
    return sinogram

def read_csv(data_name):
    data = pd.read_csv(data_name)
    return data

def define_metal(x_size, y_size, metal_to_insert):
    img_metal = np.zeros((x_size, y_size))
    
    for i in range(metal_to_insert[:,0].size):
        img_metal[metal_to_insert[i][0]:metal_to_insert[i][1], metal_to_insert[i][2]:metal_to_insert[i][3]] = 1
        
    return img_metal

def get_pixels_hu(image, dataset):
    intercept = int(dataset.RescaleIntercept)
    slope = int(dataset.RescaleSlope)
    padding = int(dataset.PixelPaddingValue)
    
    image[image <= padding] = 0
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int32)
        
    image += np.int16(intercept)
    
    return np.array(image, dtype=np.int32)

def hu2mu(img, mu_water, mu_air):
    img = img/1000.0*(mu_water-mu_air) + mu_water
    return img

def create_phantom(img, mu_water):
    xsize = img.shape[0]
    ysize = img.shape[1]
    x_array = np.linspace(-(xsize-1)/2,(xsize-1)/2, xsize)
    y_array = np.linspace(-(ysize-1)/2,(ysize-1)/2, ysize)
    X, Y = np.meshgrid(x_array, y_array)
    X_2 = np.power(X, 2)
    Y_2 = np.power(Y, 2)
    phantom = (X_2 + Y_2)< np.power(200,2)
    return (phantom * mu_water)

def fill_phantom_mono(phantom, angle_num, pixel_size):
    d_water = Radon(phantom, angle_num)
    d_water = d_water * pixel_size
    y = np.exp(-d_water)
    return -np.log(y)

def fill_phantom_poly(phantom, angle_num, pixel_size, energy_composition, E0, data):
    d_water = Radon(phantom, angle_num)
    d_water = d_water * pixel_size
    total_intensity = 0
    v = np.zeros((len(energy_composition), d_water.shape[0], d_water.shape[1]))
    m0_water = data.iloc[E0]['Water']
    for i in energy_composition:
        m_water = data.iloc[i]['Water']
        intensity = data.iloc[i]['Intensity']
        d_water_tmp = d_water*(m_water/m0_water)
        y = intensity * (np.exp(-d_water_tmp))
        v[i, :, :] = y
        total_intensity = total_intensity + intensity
    poly_p = np.sum(v, 0)
    return -np.log(poly_p/total_intensity)

def define_polynom(p_mono, p_poly, polynomial_order_for_correction):
    temp = np.polynomial.polynomial.polyfit(p_poly.flatten('F'), p_mono.flatten('F'), polynomial_order_for_correction)
    corr_coeff = temp[::-1]
    return corr_coeff

def get_values_at_E0(E0, data, metal_name, metal_density):
    m0_water = data.iloc[E0]['Water']
    m0_metal = data.iloc[E0][metal_name]
    mu_water0 = m0_water * 1.0
    mu_metal0 = m0_metal * metal_density
    return [mu_water0, mu_metal0]

def threshold_based_weighting(img, T1, T2, img_metal, mu_metal0):
    w_bone = (img - T1) / (T2 - T1)
    w_bone = np.clip(w_bone, 0 , 1)
    x_bone = w_bone * img

    w_water = (T2 - img) / (T2 - T1)
    w_water = np.clip(w_water, 0 , 1)
    x_water = w_water * img
    
    x_bone[img_metal>0] = 0
    x_water[img_metal>0] = 0

    x_metal = img_metal * mu_metal0
    
    return [x_water, x_bone, x_metal]

def sinogram_preparation(x_bone, x_water, x_metal, angle_num, pixel_size):

    # Transformada direta de Radon das 3 figuras (Parallel Beam)
    d_water = Radon(x_water, angle_num)
    d_bone = Radon(x_bone, angle_num)
    d_metal = Radon(x_metal, angle_num)

    # Transformação de pixel para UUA
    d_metal = d_metal * pixel_size
    d_bone = d_bone * pixel_size
    d_water = d_water * pixel_size
    
    return [d_water, d_bone, d_metal]

def metal_poly_ray(d_water, d_bone, d_metal, data, E0, energy_composition, metal_name):
    total_intensity = 0
    v = np.zeros((len(energy_composition), d_water.shape[0], d_water.shape[1])) 
    m0_water = data.iloc[E0-1]['Water']
    m0_bone = data.iloc[E0-1]['Bone']
    m0_metal = data.iloc[E0-1][metal_name]
    
    for i in energy_composition:
        m_water = data.iloc[i]['Water']
        m_bone = data.iloc[i]['Bone']
        m_metal = data.iloc[i][metal_name]
        intensity = data.iloc[i]['Intensity']
        d_water_tmp = d_water*(m_water/m0_water)
        d_bone_tmp = d_bone*(m_bone/m0_bone)
        d_metal_tmp = d_metal*(m_metal/m0_metal)
        DRR = d_water_tmp + d_bone_tmp + d_metal_tmp
        y = intensity * (expit(-DRR))
        v[int(i), :, :] = y
        total_intensity = total_intensity + intensity
        
    return [sum(v, 0), total_intensity]

def post_metal_insertion_poly(y_poly, total_intensity_poly, corr_coeff):
    p_poly = -np.log(y_poly/total_intensity_poly) #sinograma com ruído entra aqui
    p_final_poly = np.polyval(corr_coeff, p_poly)
    return p_final_poly

def Iradon(sinogram, number_angles = 500, filter_name = 'hann', interpolation = 'nearest'):
    angles = np.linspace(0, 180, number_angles)
    fbp = iradon(sinogram, theta = angles, circle = False, filter_name = filter_name, interpolation = interpolation)
    return fbp

def post_correction(sim, pixel_size):
    sim[sim < 0] = 0
    sim = sim / pixel_size
    return sim

def mu2hu(img, mu_water, mu_air):
    img = 1000*(img-mu_water)/(mu_water-mu_air)
    return img

def read_dcm(dataset):
    return dataset.pixel_array

def insertArtifact(dataset, polynomial_order_for_correction = 3, angle_num = 1000, 
                   pixelsPos = [150, 170, 175, 190]):
    img_original = read_dcm(dataset)

    pixel_size = float(dataset.PixelSpacing[0])

    # Valores de energia por intensidade -> energy_composition
    csvFileName = 'xray_characteristic_data.csv'
    data = read_csv(csvFileName)
    
    # Array contendo índice das intensidades
    energy_composition = np.linspace(0,119, 120).astype(int)
    
    # Energia inicial (referência) do CT -> E0
    E0 = 40
    
    # Mu do ar -> mu_air
    mu_air = 0
    
    # Mu da água na energia inicial -> mu_water
    mu_water = data.iloc[E0]['Water']
    
    # Nome do metal -> metal_name
    metal_name = 'Iron'
    
    # Densidade do metal -> metal_density
    metal_density = 6
    
    # Limiares dos tecidos moles -> T1; e dos duros -> T2
    T1 = 100
    T2 = 1500
    
    # Ordem do polinômio para correção da água -> polynomial_order_for_correction
    
    
    # Escada do ruído de Poisson -> noise_scale
    #noise_scale = 12
    
    # Nº de ângulos para a RPF -> angle_num
    
    
    
    x_size = img_original[0, :].size
    y_size = img_original[:, 0].size
    metal_to_insert = np.array([pixelsPos])
    
    img_metal = define_metal(x_size, y_size, metal_to_insert)
    img_pre = get_pixels_hu(img_original, dataset)
    img_mu = hu2mu(img_pre, mu_water, mu_air)
    
    phantom = create_phantom(img_mu, mu_water)
    p_mono = fill_phantom_mono(phantom, angle_num, pixel_size)
    p_poly = fill_phantom_poly(phantom, angle_num, pixel_size, energy_composition, E0, data)
    corr_coeff = define_polynom(p_mono, p_poly, polynomial_order_for_correction)
    mu_water0, mu_metal0 = get_values_at_E0(E0, data, metal_name, metal_density)
    
    # Transformar limiares de HU para Coef. Atenuação
    T1 = hu2mu(T1, mu_water0, mu_air)
    T2 = hu2mu(T2, mu_water0, mu_air)
    
    x_water, x_bone, x_metal = threshold_based_weighting(img_mu, T1, T2, img_metal, mu_metal0)
    d_water, d_bone, d_metal = sinogram_preparation(x_bone, x_water, x_metal, angle_num, pixel_size)
    y, total_intensity = metal_poly_ray(d_water, d_bone, d_metal, data, E0, energy_composition, metal_name)
    final_poly = post_metal_insertion_poly(y, total_intensity, corr_coeff)
    sim = Iradon(final_poly, angle_num)
    img_mu_final = post_correction(sim, pixel_size)
    final_img = mu2hu(img_mu_final, mu_water, mu_air)
    
    return final_img, img_metal