# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt
# from PyQt5.QtCore import QTimer


import cv2
import os, sys
import time
import pydicom
import numpy as np
# import threading
from scipy.io import savemat, loadmat

from skimage.transform import radon, iradon

from marGUI import Ui_MainWindow
from marInsertArtifact import insertArtifact
import marInterpolation as interp
            
class marController(object):   
    def __init__(self, app):
        self.app = app
        self.gui = None
        self.MainWindow = None

        self.last_img = None
        self.original_img = None
        self.previous_img = None
        self.dcm_content = None
        self.metal_img = None
        self.previous_metal = None
        
        self.interpDict = None
        self.historyString = ''

        self.number_angles = 500
        self.sinogram_prior = None

        self.lastState = None
        self.sinogram = False

        self.loadThread = None
        self.loadingStatus = False
        self.loadingPopup = None

        
    def addToHistory(self, string):
        self.historyString += string + '\n'
        newActionLabel = QtWidgets.QLabel(string)
        self.gui.historyVerticalLayout.addWidget(newActionLabel)
        self.gui.scrollAreaWidgetContents.setLayout(self.gui.historyVerticalLayout) 
        
        self.gui.ExportStatuslabel.setVisible(False) 
        self.gui.ExportStatuslabel.setEnabled(False)
        self.gui.saveStatusLabel.setVisible(False) 
        self.gui.saveStatusLabel.setEnabled(False)

    def imgToUint8(self, image):
        intImage = np.copy(image).astype(float)
        intImage -= image.min()
        intImage *= 255 / intImage.max()
        intImage = intImage.astype(np.uint8)
        return intImage
        
    def housfieldToFloat(self, dcmDataset):
        dcmImage = dcmDataset.pixel_array
        intercept = int(dcmDataset.RescaleIntercept)
        slope = int(dcmDataset.RescaleSlope)
        padding = int(dcmDataset.PixelPaddingValue)
        dcmImage[dcmImage <= padding] = 0
        if slope != 1:
            dcmImage = slope * dcmImage.astype(np.float64) 
        dcmImage += np.int16(intercept)
        dcmImage = dcmImage.astype(float)
        return dcmImage

        
    def read_dcm(self, img_name):
        dataset = pydicom.read_file(img_name)
        return self.housfieldToFloat(dataset)

    def read_complete_dcm(self, img_name):
        dataset = pydicom.read_file(img_name)
        return dataset
    
    def Radon(self, image, number_angles = 500):
        angles = np.linspace(0, 180, number_angles)
        sinogram = radon(image, theta = angles, 
                         circle = False, preserve_range = True)
        return sinogram

    # Inverse Radon Transform
    def Iradon(self, sinogram, number_angles = 500, filter_name = 'ramp', interpolation = 'linear'):
        angles = np.linspace(0, 180, number_angles)
        fbp = iradon(sinogram, theta = angles, filter_name = filter_name, 
                     interpolation = interpolation, circle = False)
        return fbp

    def thresholdImage(self, orig_img, threshold = 200, mode = 'Above'):
        metal_img = np.copy(orig_img)
        if(mode == "above"):
            metal_img[orig_img < threshold] = metal_img.min()
            orig_img[orig_img > threshold] = metal_img.min()
        elif(mode == "below"):
            metal_img[orig_img > threshold] = metal_img.min()
            orig_img[orig_img < threshold] = metal_img.min()
        else:
            metal_img[metal_img] = metal_img.min()
            
        metal_img = metal_img != metal_img.min()
        return orig_img, metal_img
    
    def connectUi(self):
        self.MainWindow = QtWidgets.QMainWindow()
        self.gui = Ui_MainWindow()
        self.gui.setupUi(self.MainWindow)
    
        self.gui.loadingLabel.setVisible(False)
        self.gui.labelImg = QtWidgets.QLabel(self.gui.centralwidget)
        self.gui.labelImg.setGeometry(QtCore.QRect(180, 290, 361, 271))
        self.gui.labelImg.setText("")
        self.gui.labelImg.setObjectName("labelImg")
        self.number_angles = self.gui.anglesSlider.value()
        
        self.gui.maxValueLabel.setVisible(False)
        self.gui.minValueLabel.setVisible(False)
        
        self.gui.rmArtifactButton.clicked.connect(self.on_clicked_rmArtifactButton)
        self.gui.anglesSlider.valueChanged.connect(self.on_changed_anglesSlider)
        self.gui.sliderPosSpinBox.valueChanged.connect(self.on_changed_sliderSpinBox)
        self.gui.reconstructionButton.clicked.connect(self.on_clicked_reconstructionButton)
        self.gui.iradonRadioButton.clicked.connect(self.on_clicked_iradon)
        self.gui.radonRadioButton.clicked.connect(self.on_clicked_radon)

        self.gui.addArtifactButton.clicked.connect(self.on_clicked_addArtifact)
        self.gui.undoThresholdButton.clicked.connect(self.on_clicked_undoThreshold)
        self.gui.aboveRadioButton.clicked.connect(self.on_clicked_above)
        self.gui.belowRadioButton.clicked.connect(self.on_clicked_below)

        self.gui.interpolateButton.clicked.connect(self.on_clicked_interpolateButton)
        self.gui.notConvertCheckBox.clicked.connect(self.on_clicked_notConvert)
        
        self.gui.importNextButton.clicked.connect(self.on_clicked_next)
        self.gui.transformNextButton.clicked.connect(self.on_clicked_next)
        self.gui.addArtifactNextButton.clicked.connect(self.on_clicked_next)
        self.gui.reduceArtifactNextButton.clicked.connect(self.on_clicked_next)
        self.gui.interpolateNextButton.clicked.connect(self.on_clicked_next)
        
        self.gui.transformPreviousButton.clicked.connect(self.on_clicked_previous)
        self.gui.addArtifactPreviousButton.clicked.connect(self.on_clicked_previous)
        self.gui.thresholdPreviousButton.clicked.connect(self.on_clicked_previous)
        self.gui.interpolatePreviousButton.clicked.connect(self.on_clicked_previous)
        self.gui.savePreviousButton.clicked.connect(self.on_clicked_previous)
                                                       
        self.gui.ImportButton.clicked.connect(self.on_clicked_importButton)
        self.gui.importAllButton.clicked.connect(self.on_clicked_importAll)
        self.gui.exportHistoryButton.clicked.connect(self.on_clicked_exportHistory)
        self.gui.clearHistoryButton.clicked.connect(self.on_clicked_clearHistory)
        self.gui.exportAllButton.clicked.connect(self.on_clicked_exportAll)
        self.gui.finishButton.clicked.connect(self.on_clicked_saveFig)
        self.gui.restartButton.clicked.connect(self.on_clicked_restart)
        
        
        scrollBar = self.gui.historyScrollArea.verticalScrollBar()
        scrollBar.rangeChanged.connect(lambda: scrollBar.setValue(scrollBar.maximum()))
        
        QtCore.QMetaObject.connectSlotsByName(self.MainWindow)
        
        return self.MainWindow
        
    def on_clicked_restart(self):
        os.execl(sys.executable, sys.executable, *sys.argv)
        
    def updateImage(self, image):
        self.last_img = np.copy(image)
        self.gui.maxValueLabel.setText('Max: ' + str(round(np.asarray(image).max(), 2)))
        self.gui.minValueLabel.setText('Min: ' + str(round(np.asarray(image).min(), 2)))

        if('uint8' not in str(image.dtype)):
            image = self.imgToUint8(image) 
        
        if((not self.sinogram) and (not self.gui.notConvertCheckBox.isChecked())):
            image[image == image.min()] = image.max()
            image = self.imgToUint8(image)  
        
        size = image.shape
        step = int(image.size / size[0])
        qformat = QImage.Format_Indexed8

        if len(size) == 3:
            if size[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        
        qImg = QImage(image, size[1], size[0], step, qformat)
        qImg = qImg.rgbSwapped()


        pixmap = QPixmap.fromImage(qImg)
        height = self.gui.imageLabel.height()
        width = self.gui.imageLabel.width()
        pixmap = pixmap.scaled(width, height, Qt.KeepAspectRatio) 
        
        self.gui.labelImg.setText("")
        self.gui.imageLabel.setPixmap(pixmap)


    def imageLoaded(self, enabled):
        self.gui.importNextButton.setEnabled(enabled)
        self.gui.notConvertCheckBox.setEnabled(enabled)
        if(enabled):
            self.gui.importStatusLabel.setText('Completed!')
            self.gui.importStatusLabel.setStyleSheet('color: darkGreen')
            self.gui.importStatusLabel.setVisible(True)
        else:
            self.gui.importStatusLabel.setText('')
            self.gui.importStatusLabel.setVisible(False)
             
        self.gui.rmArtifactButton.setEnabled(enabled)
        self.gui.thresholdLabel.setEnabled(enabled)
        self.gui.thresholdText.setEnabled(enabled)
        
        self.gui.radonRadioButton.setEnabled(enabled)

        self.gui.anglesLabel.setEnabled(enabled)
        self.gui.anglesSlider.setEnabled(enabled)
        self.gui.sliderPosSpinBox.setEnabled(enabled)
        self.gui.transformNextButton.setEnabled(enabled)
        self.gui.reconstructionButton.setEnabled(False)
        self.gui.iradonRadioButton.setEnabled(False)
        self.gui.filterIradonComboBox.setEnabled(False)
        self.gui.filterLabel.setEnabled(False)
        self.gui.interpolationLabel.setEnabled(False)
        self.gui.interpolationIradonComboBox.setEnabled(False)
        
        
        self.gui.thresholdLabel.setEnabled(enabled)
        self.gui.thresholdText.setEnabled(enabled)
        self.gui.reduceArtifactNextButton.setEnabled(enabled)

        self.gui.aboveRadioButton.setEnabled(enabled)
        self.gui.belowRadioButton.setEnabled(enabled)
        
        self.gui.filenameText.setEnabled(enabled)
        self.gui.filenameLabel.setEnabled(enabled)
        self.gui.restartButton.setEnabled(enabled)
        self.gui.finishButton.setEnabled(enabled)
        self.gui.exportAllButton.setEnabled(enabled)
        
        isDcmImg = not (self.dcm_content is None)
        self.gui.addArtifactButton.setEnabled(isDcmImg)
        self.gui.addArtifactLabel.setEnabled(False)
        
        self.gui.initialPosLabel.setEnabled(isDcmImg)
        self.gui.xInitialLabel.setEnabled(isDcmImg)
        self.gui.xInitialPositionBox.setEnabled(isDcmImg)
        self.gui.xFinalLabel.setEnabled(isDcmImg)
        self.gui.xFinalPositionBox.setEnabled(isDcmImg)

        self.gui.finalPosLabel.setEnabled(isDcmImg)
        self.gui.yInitialLabel.setEnabled(isDcmImg)
        self.gui.yInitialPositionBox.setEnabled(isDcmImg)
        self.gui.yFinalLabel.setEnabled(isDcmImg)
        self.gui.yFinalPositionBox.setEnabled(isDcmImg)
        
        self.gui.maxValueLabel.setVisible(enabled)
        self.gui.minValueLabel.setVisible(enabled)
        self.gui.actualImgCheckBox.setEnabled(enabled)
        
    def restartGUI(self):
        self.last_img = None
        self.original_img = None
        self.previous_img = None
        self.metal_img = None
        self.previous_metal = None
        self.dcm_content = None
        self.interpDict = None
        self.sinogram_prior = None
        self.sinogram = False
        
        self.gui.interpolateButton.setEnabled(False)
        self.gui.interpolationComboBox.setEnabled(False)
        self.gui.interpolateNextButton.setEnabled(False)
        self.gui.anglesSlider.setEnabled(True)
        self.gui.undoThresholdButton.setEnabled(False)
        self.gui.maxValueLabel.setVisible(False)
        self.gui.minValueLabel.setVisible(False)
        
        
    def on_clicked_importButton(self):  
        self.loadingStatus = True
        
        abs_path = os.path.abspath('')
        options = QtWidgets.QFileDialog.Options()
        img_path, _ = QtWidgets.QFileDialog.getOpenFileName(MainWindow,
    	"Select an image", abs_path,
        "Images (*.png *.xpm *.jpg *.npy *.jpeg *.dcm);; All Files (*);; Dicom (*.dcm)",  
        options = options)

        # path/to/image.png
        if(len(img_path) == 0):
            return
        
        self.restartGUI()
        img_file = img_path.split("/")[len(img_path.split("/")) - 1]
        img_path = img_path[0 : len(img_path) - len(img_file)]

        if(len(img_path) > 0):
            os.chdir(img_path)
            
        if('dcm' in img_file.split('.')[1]):
            image = self.read_dcm(img_file)
            self.dcm_content = self.read_complete_dcm(img_file)
    
        elif('npy' in img_file.split('.')[1]):
            image = np.load(img_file)
        else:
            image = cv2.imread(img_file, cv2.IMREAD_GRAYSCALE)

        self.original_img = np.copy(image)

        os.chdir(abs_path)

        self.updateImage(image)
        self.imageLoaded(True)
        
        self.addToHistory('Import Image: ' + img_file)
        self.loadingStatus = False

    def on_clicked_notConvert(self):
        self.updateImage(self.last_img)
        
    def on_clicked_rmArtifactButton(self):
        self.loadingStatus = True
        self.previous_img = np.copy(self.last_img)
        self.previous_metal = np.copy(self.metal_img)

        if(self.gui.aboveRadioButton.isChecked()):
            mode = "above"
        else:
            mode = "below"
            
        filtered_image, metal_image = self.thresholdImage(self.last_img, 
                                             int(self.gui.thresholdText.text()),
                                             mode = mode)
        try:
            if(np.sum(self.metal_img) != np.sum(metal_image)):
                self.metal_img += metal_image
            else: 
                return
        except:
             self.metal_img = metal_image

        self.updateImage(filtered_image)
        self.gui.undoThresholdButton.setEnabled(True)
        self.gui.interpolateButton.setEnabled(True)
        self.gui.interpolationComboBox.setEnabled(True)

        self.gui.interpolateButton.setEnabled(True)
        self.gui.interpolationComboBox.setEnabled(True)
        self.gui.interpolateNextButton.setEnabled(True)
        
        if(self.gui.aboveRadioButton.isChecked()):
            self.addToHistory('Threshold(above): ' + self.gui.thresholdText.text())
        else:
            self.addToHistory('Threshold(below): ' + self.gui.thresholdText.text())
        self.loadingStatus = False
        
    def on_clicked_undoThreshold(self):
        self.updateImage(self.previous_img)
        self.metal_img = np.copy(self.previous_metal)
        self.previous_metal = None
        self.previous_img = None
        
        if(type(self.metal_img) == type(None)):
            self.gui.interpolationComboBox.setEnabled(False)
            self.gui.interpolateButton.setEnabled(False)
            
        self.gui.undoThresholdButton.setEnabled(False)
        self.addToHistory('Undo Threshold')

    def getInterp(self, method, image = None, metal_mask = None):
        if(image is None):
            image = self.last_img
        if(metal_mask is None):
            metal_mask = self.metal_img
        
        if("Shannon" in method): 
            image[image == True] = 1
            image[image == False] = 0
            interpolated_image = interp.interp_shannon(image, metal_mask)
            interpolated_image = np.asarray(interpolated_image)
        elif("Nearest (griddata)" in method): 
            interpolated_image = interp.interp2(image, metal_mask, method = 'nearest')
        elif("Linear (griddata)" in method): 
            interpolated_image = interp.interp2(image, metal_mask, method = 'linear')
        elif("Cubic (griddata)" in method): 
            interpolated_image = interp.interp2(image, metal_mask, method = 'cubic')
        elif("Nearest (interp1d)" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'nearest')
        elif("Nearest UP (interp1d)" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'nearest-up')
        elif("Linear (interp1d)" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'linear')
        elif("Previous pixel (interp1d)" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'previous')
        elif("Next pixel (interp1d)" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'next')
        elif("Spline (CubicSpline)" in method): 
            interpolated_image = interp.spline(image, metal_mask)
        elif("Spline (0th, S)" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'zero')
        elif("Spline (1st, S\')" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'slinear')
        elif("Spline (2nd, S\'\')" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'quadratic')
        elif("Spline (3rd, S\'\'\')" in method): 
            interpolated_image = interp.interp1(image, metal_mask, kind = 'cubic')
        #default
        else: 
            interpolated_image = interp.interp2(image, metal_mask, method = 'linear')
        return interpolated_image

    def on_clicked_interpolateButton(self):
        self.loadingStatus = True
        
        method = str(self.gui.interpolationComboBox.currentText())
        if(len(method) == 0):
            method = "Linear (griddata)"
        interpolated_image = self.getInterp(method)

        self.updateImage(interpolated_image)
        self.addToHistory('Interpolate: ' + method)
        self.loadingStatus = False
        
    def on_clicked_reconstructionButton(self):   
        self.loadingStatus = True
        self.previous_img = None
        self.previous_metal = None
        self.gui.undoThresholdButton.setEnabled(False)
        self.gui.reconstructionButton.setEnabled(False)
        
        if(self.gui.iradonRadioButton.isChecked()):
            self.sinogram = False
            filter_iradon = self.gui.filterIradonComboBox.currentText()
            interp_iradon = self.gui.interpolationIradonComboBox.currentText()
            
            if('None' in filter_iradon):
                filter_iradon = None
            elif(len(filter_iradon) == 0):
                filter_iradon = 'ramp'
                
            if(len(interp_iradon) == 0):
                interp_iradon = 'linear'
                
            image = self.Iradon(self.last_img, number_angles = self.number_angles, 
                                filter_name = filter_iradon, interpolation = interp_iradon)
            if(self.metal_img is not None):
                self.metal_img = self.Iradon(self.metal_img, number_angles = self.number_angles, 
                                             filter_name = filter_iradon, interpolation = interp_iradon)
                # self.metal_img = self.metal_img != 0
            self.gui.radonRadioButton.setEnabled(True)
            self.gui.iradonRadioButton.setEnabled(False)
            self.gui.filterIradonComboBox.setEnabled(False)
            self.gui.interpolationIradonComboBox.setEnabled(False)
            self.gui.interpolationLabel.setEnabled(False)
            self.gui.filterLabel.setEnabled(False)
            self.gui.anglesSlider.setEnabled(True)
            self.gui.sliderPosSpinBox.setEnabled(True)

            
            if(filter_iradon):
                self.addToHistory('Iradon: ' + str(self.number_angles)+ ', ' 
                                  + filter_iradon + ', ' + interp_iradon)
            else: 
                self.addToHistory('Iradon: ' + str(self.number_angles)+ ', ' 
                                  + 'None' + ', ' + interp_iradon)
            
        elif(self.gui.radonRadioButton.isChecked()):
            self.sinogram = True
            image = self.Radon(self.last_img, 
                               number_angles = self.number_angles)  
            if(self.metal_img is not None):
                self.metal_img = self.Radon(self.metal_img, 
                                            number_angles = self.number_angles) 
                
            self.gui.radonRadioButton.setEnabled(False)
            self.gui.iradonRadioButton.setEnabled(True)
            self.gui.filterIradonComboBox.setEnabled(True)
            self.gui.interpolationIradonComboBox.setEnabled(True)
            self.gui.interpolationLabel.setEnabled(True)
            self.gui.filterLabel.setEnabled(True)
            self.gui.anglesSlider.setEnabled(False)
            self.gui.sliderPosSpinBox.setEnabled(False)
            self.addToHistory('Radon: ' + str(self.number_angles))
        else:
            print("you have to choose between radon and iradon.")
            return;
        self.updateImage(image) 
        self.loadingStatus = False
        
    def on_clicked_addArtifact(self):
        self.loadingStatus = True
        
        pixelsPos = []
        pixelsPos += [self.gui.yInitialPositionBox.value()]
        pixelsPos += [self.gui.yFinalPositionBox.value()]
        pixelsPos += [self.gui.xInitialPositionBox.value()]
        pixelsPos += [self.gui.xFinalPositionBox.value()]
        imgWithArtifact, metal_image = insertArtifact(dataset = self.dcm_content, 
                                                         pixelsPos = pixelsPos)

        new_max = imgWithArtifact.max() 
        old_max = self.original_img.max()
        if(self.sinogram):
            imgWithArtifact = self.Radon(imgWithArtifact, number_angles = self.number_angles)  
            metal_image = self.Radon(metal_image, number_angles = self.number_angles) 
        
        if(new_max > old_max + 10):
            imgWithArtifact[imgWithArtifact > old_max + 10] = old_max + 10
            
        metal_image = metal_image != metal_image.min()
        try:
            self.metal_img += metal_image
        except:
             self.metal_img = metal_image
             
        self.updateImage(imgWithArtifact)
        
        self.gui.interpolateButton.setEnabled(True)
        self.gui.interpolationComboBox.setEnabled(True)
        self.gui.interpolateNextButton.setEnabled(True)
        
        self.gui.addArtifactLabel.setEnabled(True)
        self.gui.addArtifactLabel.setText('Artifact Successfully added!')
        self.gui.addArtifactLabel.setStyleSheet('color: darkGreen')
        self.addToHistory('Add Artifact: ' + str(pixelsPos))
        self.loadingStatus = False        
        
    def on_clicked_radon(self):
        self.gui.iradonRadioButton.setChecked(False)
        self.gui.reconstructionButton.setEnabled(True)
        
    def on_clicked_iradon(self):
        self.gui.reconstructionButton.setEnabled(True)
        self.gui.radonRadioButton.setChecked(False)
        
    def on_changed_anglesSlider(self):
        self.gui.sliderPosSpinBox.setValue(self.gui.anglesSlider.value())
        self.number_angles = self.gui.anglesSlider.value()
        
    def on_changed_sliderSpinBox(self):
        self.gui.anglesSlider.setValue(self.gui.sliderPosSpinBox.value())
        self.number_angles = self.gui.anglesSlider.value()
    
    def on_clicked_next(self):
        self.gui.tabsControl.setCurrentIndex(self.gui.tabsControl.currentIndex() + 1)

    def on_clicked_previous(self):
        self.gui.tabsControl.setCurrentIndex(self.gui.tabsControl.currentIndex() - 1)

    def on_clicked_above(self):
        self.gui.belowRadioButton.setChecked(False)
        
    def on_clicked_below(self):
        self.gui.aboveRadioButton.setChecked(False)

    def on_clicked_saveFig(self):   
        self.loadingStatus = True
        filename = self.gui.filenameText.text()
        if((len(filename.split('.')) == 1) or ('.npy' in filename)):
            print(len(filename.split('.')))
            np.save(filename, self.last_img)
        else:
            figToSave = self.imgToUint8(self.last_img)
            cv2.imwrite(filename, figToSave)
            
        self.gui.saveStatusLabel.setText('Image Saved!')
        self.gui.saveStatusLabel.setStyleSheet('color: darkGreen')
        self.gui.saveStatusLabel.setVisible(True) 
        self.gui.saveStatusLabel.setEnabled(True)   
        self.addToHistory('Save Image: ' + filename)
        self.loadingStatus = False  
        
    def on_clicked_exportHistory(self):
        filename = "history" + str(int(time.time())) + ".txt"
        
        if(len(self.historyString) == 0):
            self.gui.ExportStatuslabel.setText('History Empty.')
            self.gui.ExportStatuslabel.setStyleSheet('color: darkRed')
            self.gui.ExportStatuslabel.setVisible(True) 
            self.gui.ExportStatuslabel.setEnabled(True)        
            self.addToHistory('Error: History empty')
        else:
            with open(filename, "w") as text_file:
                text_file.write(self.historyString)
                
            self.addToHistory('Export History: ' + filename)
            self.gui.ExportStatuslabel.setText('History Saved!')
            self.gui.ExportStatuslabel.setStyleSheet('color: darkGreen')
            self.gui.ExportStatuslabel.setVisible(True) 
            self.gui.ExportStatuslabel.setEnabled(True)
            
    def on_clicked_clearHistory(self):
        self.historyString = ''
        self.clearLayout(self.gui.historyVerticalLayout, pos = 0)
        self.addToHistory('')
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gui.historyVerticalLayout.addItem(spacerItem)
        
    def clearLayout(self, layout, pos = 0):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(pos)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.clearLayout(item.layout())
        
    def on_clicked_exportAll(self):
        imageDataMat = {}
        imageDataMat['originalImage'] = self.original_img
        imageDataMat['finalImage'] = self.last_img
        if(self.isDcmImg):
            imageDataMat['dcmContent'] = self.dcm_content
        imageDataMat['history'] = self.historyString
        
        filename = "marData" + str(int(time.time())) + ".mat"
        savemat(filename, imageDataMat)

        self.addToHistory('Export All: ' + filename)
        
    def on_clicked_importAll(self):
        abs_path = os.path.abspath('')
        options = QtWidgets.QFileDialog.Options()
        img_path, _ = QtWidgets.QFileDialog.getOpenFileName(MainWindow,
                                        	"Select an image", abs_path,
                                            "MAT Files (*.mat)",  
                                            options = options)
        # path/to/image.png
        if(len(img_path) == 0):
            return
        
        img_file = img_path.split("/")[len(img_path.split("/")) - 1]
        img_path = img_path[0 : len(img_path) - len(img_file)]

        if(len(img_path) > 0):
            os.chdir(img_path)
            
        self.historyToCommands(img_file)
        os.chdir(abs_path)
        
        
    def loadingSetState(self):
        while(1):
            if(self.lastState != self.loadingStatus):
                self.lastState = self.loadingStatus

                self.gui.loadingLabel.setEnabled(self.lastState)
            time.sleep(1)
    
    def historyToCommands(self, matFilename):
        data = loadmat(matFilename)
        if(self.gui.actualImgCheckBox.isChecked() and self.last_img is not None):
            image = self.last_img
            metal_image = self.metal_img
            previous_image = self.previous_img
            previous_metal = self.previous_metal
            sinogram = self.sinogram
        else:
            image = data['originalImage']
            metal_image = None
            previous_image = None
            previous_metal = None
            sinogram = False
        
        splitHistory = data['history'][0].split('\n', 50)
        angles = self.number_angles        
        for i in range(len(splitHistory)):
            action = splitHistory[i]
            print(action)
            if(len(action) == 0):
                continue
            self.addToHistory(action)
            
            if("Import" in action):
                self.imageLoaded(True)
                previous_image = None
                previous_metal = None
                
            elif("Radon" in action):
                angles = int(action.split(': ')[1])
                image = self.Radon(image, angles)
                try:
                    metal_image = self.Radon(metal_image, angles)
                except:
                    metal_image = None
                    
                sinogram = True
                self.gui.undoThresholdButton.setEnabled(False)
                self.gui.reconstructionButton.setEnabled(False)

                self.gui.radonRadioButton.setEnabled(False)
                self.gui.iradonRadioButton.setEnabled(True)
                self.gui.filterIradonComboBox.setEnabled(True)
                self.gui.interpolationIradonComboBox.setEnabled(True)
                self.gui.interpolationLabel.setEnabled(True)
                self.gui.filterLabel.setEnabled(True)
                self.gui.anglesSlider.setEnabled(False)
                self.gui.sliderPosSpinBox.setEnabled(False)
            
            elif("Iradon" in action):
                args = action.split(': ')[1].split(', ')
                angles = int(args[0])
                filt = args[1]
                interp = args[2]
                image = self.Iradon(image, angles, filt, interp)
                try:
                    metal_image = self.Iradon(metal_image, angles, filt, interp)
                except:
                    metal_image = None
                    
                sinogram = False
                self.gui.undoThresholdButton.setEnabled(False)
                self.gui.reconstructionButton.setEnabled(False)
                
                self.gui.radonRadioButton.setEnabled(True)
                self.gui.iradonRadioButton.setEnabled(False)
                self.gui.filterIradonComboBox.setEnabled(False)
                self.gui.interpolationLabel.setEnabled(False)
                self.gui.interpolationIradonComboBox.setEnabled(False)
                self.gui.filterLabel.setEnabled(False)
                self.gui.anglesSlider.setEnabled(True)
                self.gui.sliderPosSpinBox.setEnabled(True)
            
            # Add Artifact: [175, 190, 150, 170]
            elif("Add Artifact" in action):
                metal = None
                imgWithArtifact = None
                dim = [0, 0, 0, 0]
                split1= action.split('[')[1]
                dim[0] = int(split1.split(', ')[0])
                dim[1] = int(split1.split(', ')[1])
                dim[2] = int(split1.split(', ')[2])
                dim[3] = int(split1.split(', ')[3].replace(']', ''))
                # Need to consider dicom data
                
                dcmData = pydicom.dcmread(data['dcmContent'], force=True)
                print("dcmContent: " + str(type(dcmData)))
                imgWithArtifact, metal = insertArtifact(dataset = dcmData, 
                                                          pixelsPos = dim)
                    
                new_max = imgWithArtifact.max() 
                old_max = image.max()
                if(sinogram):
                    imgWithArtifact = self.Radon(imgWithArtifact, angles) 
                    metal_image = self.Radon(metal_image, angles)
                
                if(new_max > old_max + 10):
                    imgWithArtifact[imgWithArtifact > old_max + 10] = old_max + 10
                    
                metal_image = metal_image != metal_image.min()
                try:
                    metal_image += np.copy(metal)
                except:
                    metal_image = np.copy(metal)
                    
                    
                self.gui.interpolateButton.setEnabled(True)
                self.gui.interpolationComboBox.setEnabled(True)
                self.gui.addArtifactLabel.setEnabled(True)
                
            elif("Undo" in action):
                self.gui.undoThresholdButton.setEnabled(False)
                self.gui.interpolateButton.setEnabled(False)
                self.gui.interpolationComboBox.setEnabled(False)
                image = np.copy(previous_image)
                metal_image = np.copy(previous_metal)

            elif("Threshold" in action):
                previous_image = np.copy(image)
                previous_metal = np.copy(metal_image)
                number = int(action.split(':')[1])
                if('above' in action):
                    image, metal = self.thresholdImage(image, 
                                                  number,
                                                  mode = "above")
                    
                elif('below' in action):
                    image, metal = self.thresholdImage(image, 
                                                        number,
                                                        mode = "below")
                    
                if(metal_image is None):
                    metal_image = np.copy(metal)
                else:
                    metal_image += np.copy(metal)
                self.gui.undoThresholdButton.setEnabled(True)
                self.gui.interpolateButton.setEnabled(True)
                self.gui.interpolationComboBox.setEnabled(True)
        
                self.gui.interpolateButton.setEnabled(True)
                self.gui.interpolationComboBox.setEnabled(True)
                self.gui.interpolateNextButton.setEnabled(True)
                
            elif("Export" in action):   
                # Nothing to do
                print(action) 
            elif("Interpolate" in action):
                interpolation_method = action.split(': ')[1]
                image = self.getInterp(method = interpolation_method, 
                                       image = image, metal_mask = metal_image)
            else:
                # Nothing to do
                print('Unrecognized Command: ' + action)

        self.number_angles = angles
        self.gui.anglesSlider.setValue(angles)
        self.gui.sliderPosSpinBox.setValue(angles)
        self.metal_img = metal_image
        self.historyString = data['history'][0]
        self.updateImage(image)
        self.original_img = data['originalImage']
        self.addToHistory("Imported .mat Data")
        self.previous_img = previous_image
        # self.metal_img = metal_image != 0
    

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    controller = marController(app)
    MainWindow = controller.connectUi()
    MainWindow.show()
    sys.exit(app.exec_())  
