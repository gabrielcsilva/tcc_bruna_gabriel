#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 10 14:56:09 2021

@author: bruna
"""
 # Includes
import numpy as np
from scipy.interpolate import CubicSpline, interp1d, griddata

def post_processing(image):
    image[image < -1000] = -1000
    return image

def interp1(sinogram, mask, kind = 'linear'):
    
    x_size = sinogram[:,0].size
    y_size = sinogram[0,:].size

    for j in range(y_size):
        i = np.where(mask[:, j] == 0)
        # Spline  0th ('zero'), 1st ('slinear'), 2nd (quadratic) or 3rd (cubic) order.
        # Previous ('previous') and next ('next') point of the sample.        
        # 'kind' options: ‘linear’, ‘nearest’, ‘nearest-up’, ‘zero’, ‘slinear’, ‘quadratic’, ‘cubic’, ‘previous’, or ‘next’
        cs = interp1d(i[0], sinogram[i, j][0], kind = kind)
        xs = np.setdiff1d(np.arange(x_size), i)
        sinogram[xs, j] = cs(xs)
        
    return sinogram


def interp2(image, mask, method = 'linear'):
    
    x = np.arange(image.shape[1])
    y = np.arange(image.shape[0])
    
    xx, yy = np.meshgrid(x, y)
    
    masked = np.copy(mask)
    masked = masked != 0
    
    x1 = xx[~masked]
    y1 = yy[~masked]
    img_interp = image[~masked]
    
    # method options: 'nearest', 'linear', 'cubic'
    img_interp = griddata((x1, y1), img_interp, (xx, yy), method=method)     
    
    return img_interp


#Cubic Spline
def spline(sinogram, mask):
    sin = np.copy(sinogram)
    x_size = sin[:,0].size
    y_size = sin[0,:].size

    for j in range(y_size):
        i = np.where(mask[:, j] == 0)
        cs = CubicSpline(i[0], sin[i, j][0])
        xs = np.setdiff1d(np.arange(x_size), i)
        sin[xs, j] = cs(xs)
        
    return sin

def non_consecutive(arr):
    prev = arr[0]
    first = []
    for num in arr[1:]:
        if (prev + 1) != num:
            prev = num - 1
            first.append(num)
        prev += 1
    return first

def Whittaker_Shannon_interpolation(samples, T_s, t):
    y = np.zeros(len(t))

    for i in range(len(samples)):
        y+= samples[i] * np.sinc((t-(i+1)*T_s)/(T_s))
    
    return y

def interp_shannon(sinogram_with_metal, sinogram_mask):
    y = []
    metal_on_metal = []
    metal = []
    
    sin_interp2 = np.copy(sinogram_with_metal)
    y = np.copy(sinogram_with_metal)
    
    x_size = sin_interp2[:,0].size
    y_size = sin_interp2[0,:].size

    #Para cada ângulo do sinograma
    for j in range(y_size):
        maxsize = 0
        increment = 0

        #Identifica a posição dos metais
        i = np.where(sinogram_mask[:, j] == 0)
        xs = np.setdiff1d(np.arange(x_size), i)
        metal_on_metal.append(xs)
        if(len(xs) == 0):
            continue
        #Detecta as descontinuidades (início do metal) ao longo de uma projeção
        first = non_consecutive(xs)
        first.append(xs[len(xs)-1]+1)
        #Separa cada descontinuidade em um array diferente
        for i in range(len(first)):
            aux = []
            aux = np.where(xs < first[i])
            metal_aux = xs[aux]
            xs = np.array([x for x in xs if x not in metal_aux])
            metal.insert(i, metal_aux)

        #Obtém a maior descontinuidadee i.e. o maior espaço consecutivo preenchido por metal
        for i in range(len(metal)):
            if(metal[i].size > maxsize):
                maxsize = metal[i].size

        #Define quais pontos retirar sem acertar nenhum metal i.e. o a amostragem mínima possível
        while(1):
            value = np.copy(metal[0][0])

            index = []
            #Cria array com valores menores q o espaço
            while(1):
                value = value - (maxsize+increment)
                if(value < 0):
                    break
                else:
                    index.append(value)

            value = np.copy(metal[0][0])

            #Cria array com valores maiores q o espaço
            while(1):
                value = value + (maxsize+increment)
                if(value > x_size-1):
                    break
                else:
                    index.append(value)

            t = np.sort(np.array(index)) #Organiza valores
            val = np.array([x for x in t if x in metal_on_metal[0]]) #Identifica os valores que correspondem a metal
            
            #Verifica a lista está vazia. Caso sim, break. Caso não, significa que usou posição que contém algum outro metal
            if len(val) == 0:
                break
            increment+=1

        #Define valores para a interpolação
        T_s = maxsize+increment
        samples = sin_interp2[t, j]
        t1 = np.arange(1,x_size+1)

        #Chama interpolação
        y[:, j] = Whittaker_Shannon_interpolation(samples, T_s, t1)

    #Preencher APENAS aonde está o metal
    sin_interp2[sinogram_mask != 0] = y[sinogram_mask != 0]
    
    return sin_interp2