#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import base64
from scipy.io import loadmat
import matplotlib.pyplot as plt
import cv2

def historyToCommands(matFilename = "marData1619224255338.167.mat"):
    data = loadmat(matFilename)
    original_image = [[1, 0], [0, 1]]
    original_image = data['originalImage']
    
    splitHistory = data['history'].split('\n', 50)
    
    for action in splitHistory:
        print(action)
        if("Interpolate" in action):
            
        elif("Iradon" in action):
        
        elif("Radon" in action):
    
        elif("addArtifact" in action):
        
        elif("Threshold" in action):
        
        elif("Import" in action):
        
        elif("Export" in action):
        
        else:
            print('Unrecognized Command')
    
    plt.figure()
    plt.imshow(original_image, cmap = 'gray')
    plt.show()
    
    
if __name__ == "__main__":
    historyToCommands()