\babel@toc {brazil}{}
\contentsline {chapter}{\numberline {1}Introdução}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}O problema dos artefatos metálicos no planejamento radioterápico utilizando tomografia computadorizada}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}A redução de artefatos metálicos no ambiente hospitalar}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}Objetivos}{4}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Objetivo Geral}{4}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Objetivos Específicos}{4}{subsection.1.3.2}%
\contentsline {section}{\numberline {1.4}Organização dos próximos capítulos}{5}{section.1.4}%
\contentsline {chapter}{\numberline {2}Fundamentação acerca de tomografia por projeções de raios X, retroprojeção e artefatos metálicos}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Imageamento médico}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Tomografia computadorizada}{12}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Transformada de Radon}{12}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Teorema dos cortes de Fourier}{15}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Retroprojeção filtrada}{17}{section.2.3}%
\contentsline {section}{\numberline {2.4}Artefato metálico}{22}{section.2.4}%
\contentsline {chapter}{\numberline {3}Materiais e Métodos}{26}{chapter.3}%
\contentsline {section}{\numberline {3.1}Banco de imagens de tomografia computadorizada}{26}{section.3.1}%
\contentsline {section}{\numberline {3.2}Algoritmo de reconstrução da imagem}{27}{section.3.2}%
\contentsline {section}{\numberline {3.3}Simulação de artefatos metálicos}{27}{section.3.3}%
\contentsline {section}{\numberline {3.4}Algoritmo de redução de artefatos metálicos}{28}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Linear Metal Artifact Reduction}{28}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Normalized Metal Artifact Reduction}{29}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}Redução de artefato inspirado em~\cite {MendesSilva}}{30}{subsection.3.4.3}%
\contentsline {section}{\numberline {3.5}Desenvolvimento da interface gráfica}{32}{section.3.5}%
\contentsline {section}{\numberline {3.6}Análise dos dados}{33}{section.3.6}%
\contentsline {chapter}{\numberline {4}Resultados e Discussões}{36}{chapter.4}%
\contentsline {section}{\numberline {4.1}Algoritmo de retroprojeção filtrada}{36}{section.4.1}%
\contentsline {section}{\numberline {4.2}Algoritmo de simulação de artefato metálico}{38}{section.4.2}%
\contentsline {section}{\numberline {4.3}Algoritmo de redução de artefato metálico}{42}{section.4.3}%
\contentsline {section}{\numberline {4.4}Interface gráfica}{51}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Funcionalidades implementadas}{51}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Visualização inicial da ferramenta}{52}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3}Importando imagens}{52}{subsection.4.4.3}%
\contentsline {subsection}{\numberline {4.4.4}Radon e Iradon}{54}{subsection.4.4.4}%
\contentsline {subsection}{\numberline {4.4.5}Adicionando artefatos}{55}{subsection.4.4.5}%
\contentsline {subsection}{\numberline {4.4.6}Definindo um \textit {threshold}}{57}{subsection.4.4.6}%
\contentsline {subsection}{\numberline {4.4.7}Realizando a interpolação da região alterada}{58}{subsection.4.4.7}%
\contentsline {subsection}{\numberline {4.4.8}Salvando imagem e exportando dados}{58}{subsection.4.4.8}%
\contentsline {subsection}{\numberline {4.4.9}Pontos de destaque e informações adicionais}{61}{subsection.4.4.9}%
\contentsline {subsection}{\numberline {4.4.10}Falhas identificadas e pontos de melhoria}{63}{subsection.4.4.10}%
\contentsline {chapter}{\numberline {5}Conclusão}{64}{chapter.5}%
\contentsline {section}{\numberline {.1}Descrição matemática do algoritmo de simulação de artefatos metálicos}{71}{section.Alph0.1}%
