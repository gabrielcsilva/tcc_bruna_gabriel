# 4ª Reunião de equipe - TCC

> Data: 31/08/2020

> Todos os 3 membros estavam presentes.

## Revisão da última reunião
- Propostas e soluções;
- Entregas do TCC1 e TCC2;
- Objetivos;
- Tema e título do TCC;
- Tarefas pendentes.

## O que será feito durante essa reunião
- Revisão geral sobre cortes de Fourier;
- Forma de implementação no MatLab;

### Conceito
**Tomografia computadorizada**
Quando falamos de tomografia computadorizada (TC), tudo gira em torno de 2 principais conceitos: **Transformada de Radon** e **Teorema dos cortes de Fourier**.

**Desafio:** Calcular a imagem I(x, y) a partir dos valores encontrados em $`R_\theta`$(t) para cada ângulo definido.  

### Teorema dos cortes de Fourier
**Qual a utilidade de obter esses valores $`{R_\theta(t)}`$?**
Utilizar os valores ($`L\theta`$, t) para gerar uma outra curva $`R_\theta`$(t) que descreva a densidade do material em um o fixo ao longo de t (posição).

Calculando a transformada de Fourier desse $`R_\theta(t)`$, nós obtemos uma transformada 1D real e espelhada. Essa transformada em uma dada frequência $`f_a`$ equivale ao módulo $`(a^2 + b^2)^{1/2}`$ transformada bidimensional da mesma imagem, que forma um ângulo $`\theta`$ com o eixo x, que é o mesmo ângulo entre a perpendicular da projeção e o eixo x na imagem.

Os valores da TF 1D da projeção coincidem com os valores da TF 2D da imagem.

$`\hat{R}_\theta(f) = I(f_a \cdot cos(\theta) + f_a \cdot sin(\theta))`$

#### Transformada em domínio 1D
$`\hat{R}_\theta(f) = \int_{-\infty}^{\infty}r_{\theta}(t)\cdot e^{-j2\pi ft}dt`$

#### Transformada em domínio 2D
$`\hat{R}_\theta(f) = \int_{-\infty}^{\infty} \int_{-\infty}^{\infty} r_{\theta}(t)\cdot e^{-j2\pi ft}dx dy`$

## Tarefas da semana
- Encontrar funções para implementação no Python;
- Obter uma expressão para $`R_\theta(t)`$ em termos de uma integral em uma única variável;
- Demonstrar o teorema dos cortes de Fourier (que a integral 2D da imagem equivale à integral 1D da projeção - $`\hat{R}_\theta(f)`$)
    - Tentar fazer isso no papel pra depois aproveitar esse desenvolvimento para a documentação TCC;
- Alterações no cronograma
    - Adicionar parte de correção de artefatos metálicos; 
    - Utilizar tempo de desenvolvimento de bibliotecas IRadon e Radon para desenvolver os algoritmos de redução de artefatos metálicos.
