# Reunião 

## Objetivos

- Explicação do funcionamento da radioterapia
- Desenvolvimento dos objetivos do projeto 
	- Tratar artefatos metálicos em imagens de tomografia...
	- Desenvolver uma ferramenta que possa ser utilizada sem a necessidade de adquirir licensas de softwares adicionais ou equipamentos que demandem uma grande mobilização de orçamento
	- Fornecer essa ferramenta sem onerar hospitais de forma a possibilitar seu uso em unidades públicas de saúde.
	- Implementar algoritmos de reconstrução em uma interface gráfica;
	- Trabalhar em uma interface gráfica para esse fim, de forma que pessoas sem base de programação possa utilizar (gerar essa interface amigável e algo que melhore a experiência do usuário);
- Tratar do desenvolvimento do tema
	- Estado da arte;
	- Algoritmos já existentes;
	- Algoritmo em MatLab a partir do qual o projeto irá se basear (projeto de mestrado). Verificar soluções propostas e citar o trabalho base.
	- Disponibilizar a solução já desenvolvida em MatLab em Python utilizando uma interface gráfica.

- Desenvolver:
	- Documento
		- Fundamentação teórica
		- Introdução
		- Metodologia (experimentos preliminares, já desenvolvidos em python)
	
	- Interface
		- Figma
		- Avanço da interface para teste

## TCC1 - Entrega

- Introdução completa
	- Imagem com e sem artefatos metálicos
- Objetivos
	- Argumentação 
	- Objetivos bem detalhados
- Fundamentação teórica
	- Complementar retroprojeção filtrada
	- artefatos metálicos
- Metodologia
	- Testes
	- Resultados prévios
- Resultados
	- Descrição do que já existe no mestrado feito
	- Interface pronta
	- Implementação de exemplos já em Python
- Descrição do algorítmo de redução de artefatos metálicos

### Outubro

- Rodar interface gráfica com testes já feitos
- Complementar